<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AppModel extends CI_Model
{
    public function getAll($table, $result = null)
    {
        $this->db->select('*');
        $query = $this->db->get($table);

        if ($result == 'array') {
            return $query->result_array();
        }
        return $query->result();
    }

    public function getAllProductsJoinCategory()
    {
        $this->db->select('p.*, c.name AS category_name');
        $this->db->join('category c', 'p.category = c.id');
        $query = $this->db->get('product p');
        return $query->result();
    }

    public function sizeOf($table)
    {
        return $this->db->get($table)->num_rows();
    }

    public function create($table)
    {
        $this->db->where('name', $this->input->post('name', true));
        $validate = $this->db->get($table)->num_rows();

        if ($validate == 0) {
            if ($table == 'category') {
                return $this->db->insert($table,
                    [
                        'name' =>$this->input->post('name', true)
                    ]
                );
            } else if ($table == 'product') {
                return $this->db->insert($table,
                    [
                        'category' =>$this->input->post('category', true),
                        'code' =>$this->input->post('code', true),
                        'name' =>$this->input->post('name', true),
                        'description' =>$this->input->post('description', true)
                    ]
                );
            }
            return false;
        }
        return false;
    }

    public function update($id, $table)
    {
        $this->db->where('id', $id);
        if ($table == 'category') {
            return $this->db->update($table,
                ['name' => $this->input->post('name', true)]
            );
        } else if ($table == 'product') {
            return $this->db->update($table,
                [
                    'category' => $this->input->post('category', true),
                    'code' => $this->input->post('code', true),
                    'name' => $this->input->post('name', true),
                    'description' => $this->input->post('description', true)
                ]
            );
        }
    }

    public function delete($id, $table)
    {
        return $this->db->delete($table, ['id' => $id]);
    }

    public function dropdown($table, $column = null, $order = 'asc')
    {
        if ($column && $order)
            $this->db->order_by($column, $order);
        else
            $this->db->order_by('id', 'asc');

        $result = $this->db->get($table);

        $data[''] = 'Select Category';
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $data[$row->id] = $row->name;
            }
        }
        return $data;
    }

    public function getGrid($table)
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
        $offset = ($page-1) * $rows;

        $result = array();
        $result['total'] = $this->db->get($table)->num_rows();
        $row = array();

        if ($table == 'product'){
            $this->db->select('product.*, category.name AS category_name');
            $this->db->join('category', 'product.category = category.id');
            $this->db->limit($rows,$offset);

            if ($sort == 'category_name')
                $this->db->order_by($sort,$order);
            else
                $this->db->order_by($table.'.'.$sort,$order);
        } else {
            $this->db->limit($rows,$offset);
            $this->db->order_by($table.'.'.$sort,$order);
        }
        $criteria = $this->db->get($table);

        if ($table == 'category') {
            foreach($criteria->result_array() as $data)
            {
                $row[] = array(
                    'id'=>$data['id'],
                    'name'=>$data['name']
                );
            }
        } else if($table == 'product') {
            foreach($criteria->result_array() as $data)
            {
                $row[] = array(
                    'id'=>$data['id'],
                    'code'=>$data['code'],
                    'name'=>$data['name'],
                    'description'=>$data['description'],
                    'category'=>$data['category'],
                    'category_name'=>$data['category_name']
                );
            }
        }
        $result=array_merge($result,array('rows'=>$row));
        return json_encode($result);
    }
}