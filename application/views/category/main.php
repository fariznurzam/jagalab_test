<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div data-options="region:'center'">

    <div id="toolbar">
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newCategory()">New Category</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editCategory()">Edit Category</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deleteCategory()">Remove Category</a>
    </div>

    <table class="easyui-datagrid" url="category/get?grid=true"
           id="dg" title="Categories" style="width:auto;height:auto"
           toolbar="#toolbar" pagination="true"
           rownumbers="true" fitColumns="true" singleSelect="true">

        <thead>
        <tr>
            <th data-options="field:'id'" sortable="true">ID</th>
            <th data-options="field:'name'" sortable="true">Name</th>
        </tr>
        </thead>
    </table>

    <div id="dlg" class="easyui-dialog" style="width:400px;height:auto;padding:10px 20px"
         closed="true" buttons="#dlg-buttons">
        <div class="ftitle">Update Category</div>

        <?php echo form_open('category/create', ['id' => 'fm', 'method'=>'post']); ?>
            <div class="fitem">
                <label for="name">Category Name:</label>
                <input type="text" name="name" value="<?php echo set_value('name'); ?>" class="easyui-validatebox easyui-textbox" required="true">
            </div>
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveCategory()" style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
    </div>
</div>

<script>
    var url;

    function newCategory(){
        $('#dlg').dialog('open').dialog('setTitle','New Category');
        $('#fm').form('clear');
        url = 'category/create';
    }

    function saveCategory(){
        $('#fm').form('submit',{
            url: url,
            onSubmit: function(){
                return $(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if (result.status) {
                    $('#dlg').dialog('close');        // close the dialog
                    $('#dg').datagrid('reload');    // reload the user data
                } else {
                    $.messager.show({
                        title: 'Error',
                        msg: result.msg
                    });
                }
            }
        });
    }

    function editCategory() {
        var row = $('#dg').datagrid('getSelected');
        if (row){
            $('#dlg').dialog('open').dialog('setTitle','Edit Category');
            $('#fm').form('load',row);
            url = 'category/edit/'+row.id;
        }
    }

    function deleteCategory(){
        var row = $('#dg').datagrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to remove this category?',function(r){
                if (r){
                    $.post('category/delete',{id:row.id},function(result){
                        if (result.status){
                            $('#dg').datagrid('reload');
                        } else {
                            $.messager.show({
                                title: 'Error',
                                msg: result.msg
                            });
                        }
                    },'json');
                }
            });
        }
    }
</script>