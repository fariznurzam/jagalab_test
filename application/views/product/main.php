<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div data-options="region:'center'">

    <div id="toolbar">
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newProduct()">New Product</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editProduct()">Edit Product</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deleteProduct()">Remove Product</a>
    </div>

    <table class="easyui-datagrid" url="product/get?grid=true"
           id="dg" title="Product" style="width:auto;height:auto"
           toolbar="#toolbar" pagination="true"
           rownumbers="true" fitColumns="true" singleSelect="true">

        <thead>
        <tr>
            <th data-options="field:'id'" sortable="true">ID</th>
            <th data-options="field:'code'" sortable="true">Code</th>
            <th data-options="field:'name'" sortable="true">Name</th>
            <th data-options="field:'description'" sortable="true">Description</th>
            <th data-options="field:'category_name'" sortable="true">Category</th>
        </tr>
        </thead>
    </table>

    <div id="dlg" class="easyui-dialog" style="width:400px;height:auto;padding:10px 20px"
         closed="true" buttons="#dlg-buttons">
        <div class="ftitle">Update Product</div>

        <?php echo form_open('product/create', ['id' => 'fm', 'method'=>'post']); ?>
            <br/>
            <div class="fitem">
                <label for="code">Product Code:</label>
                <input type="text" name="code" value="<?php echo set_value('code'); ?>" class="easyui-validatebox easyui-textbox" required="true">
            </div>
            <br/>
            <div class="fitem">
                <label for="name">Product Name:</label>
                <input type="text" name="name" value="<?php echo set_value('name'); ?>" class="easyui-validatebox easyui-textbox" required="true">
            </div>
            <br/>
            <div class="fitem">
                <label for="description">Product Description:</label>
                <input type="text" name="description" value="<?php echo set_value('description'); ?>" class="easyui-validatebox easyui-textbox">
            </div>
            <br/>
            <div class="fitem">
                <label for="category">Category:</label>
                <?php
                    $attributes = 'class="easyui-validatebox easyui-combobox" required="true"';
                    echo form_dropdown('category', $categories, $categories_selected, $attributes);
                ?>
            </div>
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveProduct()" style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
    </div>
</div>

<script>
    var url;

    function newProduct(){
        $('#dlg').dialog('open').dialog('setTitle','New Product');
        $('#fm').form('clear');
        url = 'product/create';
    }

    function saveProduct(){
        $('#fm').form('submit',{
            url: url,
            onSubmit: function(){
                return $(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if (result.status) {
                    $('#dlg').dialog('close');        // close the dialog
                    $('#dg').datagrid('reload');    // reload the user data
                } else {
                    $.messager.show({
                        title: 'Error',
                        msg: result.msg
                    });
                }
            }
        });
    }

    function editProduct() {
        var row = $('#dg').datagrid('getSelected');
        if (row){
            $('#dlg').dialog('open').dialog('setTitle','Edit Product');
            $('#fm').form('load',row);
            url = 'product/edit/'+row.id;
        }
    }

    function deleteProduct(){
        var row = $('#dg').datagrid('getSelected');
        if (row){
            $.messager.confirm('Confirm','Are you sure you want to remove this product?',function(r){
                if (r){
                    $.post('product/delete',{id:row.id},function(result){
                        if (result.status){
                            $('#dg').datagrid('reload');
                        } else {
                            $.messager.show({
                                title: 'Error',
                                msg: result.msg
                            });
                        }
                    },'json');
                }
            });
        }
    }
</script>