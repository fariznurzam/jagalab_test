<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

    </div>

<footer>
    Ini footer
    <div class="form_error">
        <?php echo validation_errors(); ?>
    </div>
</footer>
</div>

<style type="text/css">
    #fm{
        margin:0;
        padding:10px 30px;
    }
    .ftitle{
        font-size:14px;
        font-weight:bold;
        padding:5px 0;
        margin-bottom:10px;
        border-bottom:1px solid #ccc;
    }
    .fitem{
        margin-bottom:5px;
    }
    .fitem label{
        display:inline-block;
        width:80px;
    }
    .fitem input{
        width:160px;
    }
    .easyui-combobox{
        width: 160px;
    }
</style>

</body>


<script>
    $.parser.parse();
</script>

</html>