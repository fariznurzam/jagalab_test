<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title ?> | Jagalab</title>
    <link rel="stylesheet" type="text/css" href="<?php echo 'assets/js/jquery-easyui/themes/material/easyui.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo 'assets/js/jquery-easyui/themes/icon.css' ?>">

    <script src="<?php echo 'assets/js/jquery-easyui/jquery.min.js'; ?>"></script>
    <script src="<?php echo 'assets/js/jquery-easyui/jquery.easyui.min.js'; ?>"></script>
</head>

<body class="easyui-layout">
<div id="win" class="easyui-window" title="Jagalab App" style="width:600px;height:465px"
     data-options="iconCls:'icon-man',modal:true">

    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'north',split:true" style="height:auto; padding-bottom: 8px" >
            <div style="padding:5px;background:#fafafa;width:100%;border:1px solid #ccc">
                <a href="<?php echo 'category';?>" class="easyui-linkbutton" iconCls="icon-large-shapes">Category</a>
                <a href="<?php echo 'product';?>" class="easyui-linkbutton" iconCls="icon-large-clipart">Product</a>
            </div>
        </div>

