<?php

class Product_test extends TestCase
{
    public function test_index()
    {
        $output = $this->request('GET', 'product/index');
        $this->assertContains(
            '<title>Product Page | Jagalab</title>', $output
        );
    }

    public function test_get()
    {
        $output = $this->request('GET', 'product/get');
        $expected = [
            'id' => '1',
            'category' => '1',
            'code' => 'CI36200',
            'name' => 'Intel Core i3 3200',
            'description' => null,
            'image' => null
        ];
        $output = json_decode($output);
        $result = [
            'id' => $output[0]->id,
            'category' => $output[0]->category,
            'code' => $output[0]->code,
            'name' => $output[0]->name,
            'description' => $output[0]->description,
            'image' => $output[0]->image,
        ];
        $this->assertEquals($expected, $result);
    }

    public function test_create()
    {
        $data = ['name' => $this->generateRandomString(10),
            'code' => $this->generateRandomString(7),
            'description' => $this->generateRandomString(10),
            'category' => 1];
        $output = $this->request('POST', 'product/create', $data);
        $result = json_decode($output);
        $this->assertTrue($result->status);
    }

    /**
     * if test failed, please change $id value to another existing id in table
     */
    public function test_update()
    {
        $id = 20;
        $data = ['name' => $this->generateRandomString(10),
            'code' => $this->generateRandomString(7),
            'description' => $this->generateRandomString(10),
            'category' => 1];
        $output = $this->request('POST', sprintf('product/edit/%d', $id), $data);
        $result = json_decode($output);
        $this->assertTrue(true, $result->status);
    }

    /**
     * if test failed, please change $id value to another existing id in table
     */
    public function test_delete()
    {
        $id = 23;
        $data = ['id' => $id];
        $output = $this->request('POST', 'product/delete', $data);
        $result = json_decode($output);
        $this->assertTrue($result->status);
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}