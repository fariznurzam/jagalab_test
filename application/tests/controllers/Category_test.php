<?php

class Category_test extends TestCase
{
    public function test_index()
    {
        $output = $this->request('GET', 'category/index');
        $this->assertContains(
            '<title>Category Page | Jagalab</title>', $output
        );
    }

    public function test_get()
    {
        $output = $this->request('GET', 'category/get');
        $expected = [
            'id' => 1,
            'name' => 'Processor'
        ];
        $output = json_decode($output);
        $result = [
            'id' => $output[0]->id,
            'name' => $output[0]->name
        ];
        $this->assertEquals($expected, $result);
    }

    public function test_create()
    {
        $data = ['name' => $this->generateRandomString(10)];
        $output = $this->request('POST', 'category/create', $data);
        $result = json_decode($output);
        $this->assertTrue($result->status);
    }

    /**
     * if test failed, please change $id value to another existing id in table
     */
    public function test_update()
    {
        $id = 2;
        $data = ['name' => $this->generateRandomString(10)];
        $output = $this->request('POST', sprintf('category/edit/%d', $id), $data);
        $result = json_decode($output);
        $this->assertTrue(true, $result->status);
    }

    /**
     * if test failed, please change $id value to another existing id in table
     */
    public function test_delete()
    {
        $id = 10;
        $data = ['id' => $id];
        $output = $this->request('POST', 'category/delete', $data);
        $result = json_decode($output);
        $this->assertTrue($result->status);
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}