<?php

class Appmodel_test extends TestCase
{
    public function setUp()
    {
        $this->obj = $this->newModel('AppModel');
    }

    public function testGetAllCategoryTableAsArrayExpectedContainsSample()
    {
        $expectedCategory = [
            'id' => 1,
            'name' => 'Processor'
        ];
        $categories = $this->obj->getAll('category', 'array');
        $this->assertContains($expectedCategory, $categories);

        $expectedProduct = [
            'id' => '1',
            'category' => '1',
            'code' => 'CI36200',
            'name' => 'Intel Core i3 3200',
            'description' => null,
            'image' => null
        ];
        $products = $this->obj->getAll('product', 'array');
        $this->assertContains($expectedProduct, $products);
    }

    public function testSizeOfTableExpectedMoreThanZero()
    {
        $sizeCategory = $this->obj->sizeOf('category');
        $this->assertTrue($sizeCategory > 0);
        $this->assertNotNull($sizeCategory);

        $sizeProduct = $this->obj->sizeOf('product');
        $this->assertTrue($sizeProduct > 0);
        $this->assertNotNull($sizeProduct);
    }

    /**
     * this test succeed if return OK, but did not perform any assertions
     * if this test return failed, please change $_POST['name'] value
     */
    public function testCreate()
    {
        $_POST['name'] = $this->generateRandomString(10);
        $resultCategory = $this->obj->create('category', $_POST);
        $this->assertTrue($resultCategory);

        $_POST['category'] = 1;
        $_POST['code'] = $this->generateRandomString(7);
        $_POST['name'] = $this->generateRandomString(10);
        $resultProduct = $this->obj->create('product', $_POST);
        $this->assertTrue($resultProduct);
    }

    public function testUpdate()
    {
        $row = $this->obj->sizeOf('category') - 1;
        $_POST['name'] = $this->generateRandomString(10);
        $result = $this->obj->update($row, 'category', $_POST);
        $this->assertTrue($result);

        $row = $this->obj->sizeOf('product') - 1;
        $_POST['category'] = 1;
        $_POST['code'] = $this->generateRandomString(7);
        $_POST['name'] = $this->generateRandomString(10);
        $result = $this->obj->update($row, 'product', $_POST);
        $this->assertTrue($result);
    }

    public function testDelete()
    {
        $row = $this->obj->sizeOf('category') - 1;
        $result = $this->obj->delete($row, 'category');
        $this->assertTrue($result);

        $row = $this->obj->sizeOf('product') - 1;
        $result = $this->obj->delete($row, 'product');
        $this->assertTrue($result);
    }

    public function testDropdownCategory()
    {
        $result = $this->obj->dropdown('category');
        $expectedCategory = "Processor";
        $expectedDefault = 'Select Category';
        $this->assertEquals($expectedCategory, $result[1]);
        $this->assertEquals($expectedDefault, $result['']);
    }

    public function testGetAllProductsJoinCategory()
    {
        $expect = [
            'id' => 1,
            'code' => 'CI36200',
            'name' => 'Intel Core i3 3200',
            'description' => null,
            'image' => null,
            'category' => 'Processor'
        ];
        $result = $this->obj->getAllProductsJoinCategory();
        $actual = [
            'id' => 1,
            'code' => $result[0]->code,
            'name' => $result[0]->name,
            'description' => $result[0]->description,
            'image' => $result[0]->image,
            'category' => $result[0]->category_name
        ];
        $this->assertEquals($expect, $actual);
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}