<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = 'Product Page';

        $data['categories'] = $this->AppModel->dropdown('category', 'name');
        $data['categories_selected'] = $this->input->post('category') ? $this->input->post('category') : '';
        $this->template->template_render('product/main', $data);
    }

    public function get()
    {
        if(isset($_GET['grid'])) {
            echo $this->AppModel->getGrid('product');
        }
        else {
            $result = $this->AppModel->getAllProductsJoinCategory('product');
            echo json_encode($result);
        }

    }

    public function create()
    {
        $this->form_validation->set_rules('name', 'Nama Produk', 'required', ['required' => 'Product Name is mandatory']);
        $this->form_validation->set_rules('category', 'Kategori', 'required', ['required' => 'Category is mandatory']);
        $this->form_validation->set_rules('code', 'Kode Produk', 'required', ['required' => 'Product Code is mandatory']);

        if ($this->form_validation->run() == FALSE || !isset($_POST))
        {
            $this->index();
        }
        else
        {
            if($this->AppModel->create('product'))
                echo json_encode(['status'=>true]);
            else
                echo json_encode(['status'=>false, 'msg'=>'failed create data/ name has been used']);
        }
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('name', 'Nama Produk', 'required', ['required' => 'Name column is mandatory']);

        if ($this->form_validation->run() == FALSE || $id == null || !isset($_POST))
        {
            $this->index();
        }
        else
        {
            if($this->AppModel->update($id, 'product'))
                echo json_encode(['status'=>true]);
            else
                echo json_encode(['status'=>false, 'msg'=>'failed update data']);
        }
    }

    public function delete($id = null)
    {
        $id = intval(addslashes($_POST['id']));
        if($this->AppModel->delete($id, 'product') && isset($_POST))
            echo json_encode(['status'=>true]);
        else
            echo json_encode(['status'=>false, 'msg'=>'failed delete data']);
    }
}