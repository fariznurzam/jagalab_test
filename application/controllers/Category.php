<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = 'Category Page';
        $this->template->template_render('category/main', $data);
    }

    public function get()
    {
        if(isset($_GET['grid'])) {
            echo $this->AppModel->getGrid('category');
        }
        else {
            $result = $this->AppModel->getAll('category');
            echo json_encode($result);
        }
    }

    public function create()
    {
        $this->form_validation->set_rules('name', 'Nama Kategori', 'required', ['required' => 'Name column is mandatory']);

        if ($this->form_validation->run() == FALSE || !isset($_POST))
        {
            $this->index();
        }
        else
        {
            if($this->AppModel->create('category'))
                echo json_encode(['status'=>true]);
            else
                echo json_encode(['status'=>false, 'msg'=>'failed create data/ name has been used']);
        }
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('name', 'Nama Kategori', 'required', ['required' => 'Name column is mandatory']);

        if ($this->form_validation->run() == FALSE || $id == null || !isset($_POST))
        {
            $this->index();
        }
        else
        {
            if($this->AppModel->update($id, 'category'))
                echo json_encode(['status'=>true]);
            else
                echo json_encode(['status'=>false, 'msg'=>'failed update data']);
        }
    }

    public function delete($id = null)
    {
        $id = intval(addslashes($_POST['id']));
        if($this->AppModel->delete($id, 'category') && isset($_POST))
            echo json_encode(['status'=>true]);
        else
            echo json_encode(['status'=>false, 'msg'=>'failed delete data']);
    }
}