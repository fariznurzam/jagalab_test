# Welcome to Jagalab Assessment!

Jagalab Assessment created by Fariz.

If you would like to see how the unit test work, please follow these steps:
Note please attention that your console is in project directory

```text
cd application/tests
phpunit
```

For futher information, feel free to <a href="mailto:fariz0020@students.amikom.ac.id">Content Me</a>

# README #

What you need to understand to complete this test : 

* Standard GIT command ie : add, commit, pull, push, merge
* PHP Language : variable, array, if-else, looping, etc
* Codeigniter Framework : MVC structure best practice. Try official turotual : https://codeigniter.com/user_guide/tutorial/index.html . Important to read : Database reference, Library : Form Validation, File Uploading, Input, etc
* Javascript : variable, array, if-else, looping, etc
* Jquery : selectors, basic function like : $.show(). $.hide(), $.html(), $.ajax, $.post, $.get, 
* JSON and it's manupulation in PHP and Javscript
* Jquery EasyUI : try the demo (http://www.jeasyui.com/demo/main/index.php) & Read documentation (http://www.jeasyui.com/documentation/index.php#) to get basic understanding on how to use it 

# TEST MATERIAL #

/BACA SAYA.PDF